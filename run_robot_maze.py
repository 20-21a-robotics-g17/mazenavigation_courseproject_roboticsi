from gopigo import *
import sys
import os.path as path
import time

# Default speed of gopigo2 = 200.
set_speed(100)
prev_x, prev_y = -1, -1
prev_ori = -1


def orient_robot(prev_ori, ori):
    if prev_ori == 0:
        if ori == 1:
            turn_left(90)
        elif ori == 3:
            turn_right(90)
    elif prev_ori == 1:
        if ori == 2:
            turn_left(90)
        elif ori == 0:
            turn_right(90)
    elif prev_ori == 2:
        if ori == 3:
            turn_left(90)
        elif ori == 1:
            turn_right(90)
    elif prev_ori == 3:
        if ori == 0:
            turn_left(90)
        elif ori == 2:
            turn_right(90)

if path.exists("path_from_image.txt"):
    with open('path_from_image.txt', 'r') as f:
        for line in f:
            line = line.strip("[()]")
            line = line.rsplit("), (")
    for i in line:
        mytuple = tuple(map(int, i.split(', ')))
        new_x = mytuple[0]
        new_y = mytuple[1]
        
        if new_x > prev_x:
            ori = 0
            prev_x = new_x
            if new_y == prev_y:
                fwd()
                time.sleep(0.01)
                stop()
            elif new_y > prev_y:
                prev_ori=ori
                ori = 1
                prev_y = new_y
                print(prev_ori, ori)
                orient_robot(prev_ori, ori)
                
            elif new_y < prev_y:
                prev_ori=ori
                ori = 3
                new_y = prev_y
                print(prev_ori, ori)
                orient_robot(prev_ori, ori)
        
        elif new_x < prev_x:
            ori = 2
            new_x = prev_x
            if new_y == prev_y:
                fwd()
                time.sleep(0.01)
                stop()
            elif new_y > prev_y:
                prev_ori=ori
                ori = 1
                prev_y = new_y
                print(prev_ori, ori)
                orient_robot(prev_ori, ori)
                
            elif new_y < prev_y:
                prev_ori=ori
                ori = 3
                new_y = prev_y
                print(prev_ori, ori)
                orient_robot(prev_ori, ori)

        else:
            fwd()
            time.sleep(0.01)
            stop()
            print("Here")

        
            
        
# while True:
#     orient_robot