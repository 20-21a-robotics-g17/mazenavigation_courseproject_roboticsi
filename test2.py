import cv2
from math import atan2, degrees, atan
import numpy as np
from Marker import *

dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_250)
detectorParams = cv2.aruco.DetectorParameters_create()
detectorParams.adaptiveThreshConstant = 15


def rotate(image, angle, center=None, scale=1.0):
	(h, w) = image.shape[:2]

	if center is None:
		center = (w / 2, h / 2)

	# Perform the rotation
	M = cv2.getRotationMatrix2D(center, angle, scale)
	rotated = cv2.warpAffine(image, M, (w, h))

	return rotated

def detect(frame, draw = True):
	# This function detects all the markers from the current frame
	# The returned structure is a list, where:
	# First element (index 0) is a tuple of ([list of marker corner coordinates (as lists first element)], [type of variable the coordinates are represented in])
	# Second element is list of marker id numbers, each as a list.
	# the lists are in same order (when using coordinate list like detected[0][0][0][0][0], then this is the first coordinate of marker with id detected[1][0][0])
	detected = cv2.aruco.detectMarkers(frame, dictionary, parameters=detectorParams)

	if draw:
		# This function prints the list of markers onto the frame (their main corner, contour and id)
		cv2.aruco.drawDetectedMarkers(frame, detected[0], detected[1])

	# Convert the complicated structure used by ArUco into a more convenient form. See Marker.py
	markers = parseMarkers(detected)

	return markers

def markerById(markers, id):
	# <-- Write a function for finding a marker with a specific id from the list of all markers
	for marker in markers:
		if marker.id == id:
			break
	return marker

def center(markers, id):
	global frame
	# <-- Write a function for finding the center coordinates of a marker with a specific id
	marker = markerById(markers, id)
	x = int(marker.x1 + (marker.x3 - marker.x1)/2)
	y = int(marker.y1 + (marker.y3 - marker.y1)/2)

	# dot = marker.x1 * marker.x3 + marker.y1 * marker.y3  # dot product
	# det = marker.x1 * marker.y3 - marker.y1 * marker.x3  # determinant
	mtan = (marker.y3-marker.y1)/(marker.x3-marker.x1)
	# angle = degrees(atan2(det, dot))  # atan2(y, x) or atan2(sin, cos) angle between -180 and 180
	angle = degrees(atan(mtan))


	#visualise
	# arrow
	px = int(x + 2 * 20 * np.sin(np.radians(angle)))
	py = int(y - 2 * 20 * np.cos(np.radians(angle)))
	cv2.arrowedLine(frame, (x, y), (px, py), (255, 255, 255), 2, tipLength=.2)

	return [x, y, angle]

if __name__ == "__main__":

	# read the image from file
	frame = cv2.imread('test_image2a.png', 0)
	frame = rotate(frame, 80)
	cv2.imshow('original', frame)

	markers = detect(frame)

	# <-- Here, find the center of the robot and print the coordinates
	cc = center(markers, 70)
	print(cc)
	cv2.circle(frame, tuple(cc[:2]), 10, (255,255,255), thickness=-1)


	# Display the resulting image with markers
	cv2.imshow('markers', frame)

	# Quit the program when any key is pressed
	cv2.waitKey(0)

	# When everything done, release the capture
	print('closing program')
	cv2.destroyAllWindows()
