import heapq
import cv2
import numpy as np
import os


class Node:
    """
    A node class for A* Pathfinding
    """

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0
        self.h = 0
        self.f = 0


    def __eq__(self, other):
        return self.position == other.position
    
    # What is going to be printed when printing class Node object
    def __repr__(self):
        return f"Position: {self.position} - g: {self.g} h: {self.h} f: {self.f}"

    # defining less than for purposes of heap queue
    def __lt__(self, other):
        return self.f < other.f
    
    # defining greater than for purposes of heap queue
    def __gt__(self, other):
        return self.f > other.f


class Astar:
    """Astar class for PathFinding.
    
    init Args:
    
        start (tuple, list of int): (x, y) coords of start position. 
        IMPORTANT!: if coords taken from pixel coords of image, reverse them: (x, y) = (y, x)

        end (tuple, list of int): (x, y) coords of end position. 
        IMPORTANT!: if coords taken from pixel coords of image, reverse them: (x, y) = (y, x)

        img (numpy.ndarray): image of maze 
        
    """

    def __init__(self, start, end, img, scale):
        
        # Start and End node with start, end coords. Look ==> class Node()
        self.start_node = Node(None, start)
        self.start_node.g = self.start_node.h = self.start_node.f = 0
        self.end_node = Node(None, end)
        self.end_node.g = self.end_node.h = self.end_node.f = 0
        
        # Open and closed list
        self.open_list = []
        self.closed_list = []
        
        # scale (float): for interpolating (shrinking) img_orig == faster astar
        # maze (numpy.ndarray):  2D matrix
        # path (list of tuples): [(#, #), (#, #), (#, #), ...]
        self.scale = scale
        self.img_orig = img       
        self.maze = None
        self.path = []



    def return_path(self, current_node):

        current = current_node

        while current is not None:
            self.path.append(current.position)
            current = current.parent
        
        self.path = self.path[::-1]


    # TODO: Handle the thesholding based on image type (grayscale, RGB, ...)
    def img_to_maze(self):
        
        
        # Interpolation (shrinking)
        ##width = int(self.img_orig.shape[1] * self.scale)
        ##height = int(self.img_orig.shape[0] * self.scale)
        ##dim = (width, height)
        ##img_resized = cv2.resize(self.img_orig, dim, interpolation = cv2.INTER_AREA)
        #img_gray = cv2.cvtColor(img_resized, cv2.COLOR_BGR2GRAY)

        ret, thresholded = cv2.threshold(self.img_orig, 216, 255, cv2.THRESH_BINARY)

        # Morphology for adjusting walls and path sizes, removing artifacts 
        ##kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        ##thresholded = cv2.dilate(thresholded, kernel, iterations=1)
        ##thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, kernel)

        #cv2.imwrite("th.png", thresholded)
        
        self.maze = thresholded/255.0

        
        
    def save_path_img(self):

        # Scale path coords to fit on original image, as they are on interpolated (shrinked)
        new_path = list()
        for x, y in self.path:
            new_coord = (int(x / self.scale), int(y / self.scale))
            new_path.append(new_coord)

        # Draw path
        # TO-DO: make variables for colors, thickness (?...)
        for coord in new_path:
            img_solved = cv2.circle(self.img_orig, coord[::-1], 3, (0, 255, 0), -1)
        
        # Saving image
        cwd = os.getcwd()
        cv2.imwrite("solved.png", img_solved)
        
        print("Image saved!")
        
        
        
    def draw_inter_path(self, img):

        # Scale path coords to fit on original image, as they are on interpolated (shrinked)
        new_path = list()
        for x, y in self.path:
            new_coord = (int(x / self.scale), int(y / self.scale))
            new_path.append(new_coord)

        print(new_path)
        # Draw path
        # TO-DO: make variables for colors, thickness (?...)
        for coord in new_path:
            img_solved = cv2.circle(img, coord[::-1], 3, (0, 255, 0), -1)
        
        print("Showing drawn path")
        cv2.imshow("MAZE SOLUTION", img_solved)
        cv2.waitKey(0)



    def solve(self):

        heapq.heapify(self.open_list) 
        heapq.heappush(self.open_list, self.start_node)

        adjacent_squares = ((0, -1), (0, 1), (-1, 0), (1, 0),)

        while len(self.open_list) > 0:
        
            # Get the current node
            current_node = heapq.heappop(self.open_list)
            self.closed_list.append(current_node)

            # Found the goal
            if current_node == self.end_node:
                self.return_path(current_node)
                break

            # Generate children
            children = []
            
            for new_position in adjacent_squares: # Adjacent squares

                # Get node position
                node_position = tuple(np.add(current_node.position, new_position))
                #node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

                # Make sure within range
                # TO-DO: Looks ugly, rewrite.
                if node_position[0] > (len(self.maze) - 1) or node_position[0] < 0 or node_position[1] > (len(self.maze[len(self.maze)-1]) -1) or node_position[1] < 0:
                    continue

                # Make sure walkable terrain
                if self.maze[node_position] != 0:
                    continue

                # Create new node
                new_node = Node(current_node, node_position)

                # Append
                children.append(new_node)

            # Loop through children
            for child in children:
                # Child is on the closed list
                if len([closed_child for closed_child in self.closed_list if closed_child == child]) > 0:
                    continue

                # Create the f, g, and h values
                child.g = current_node.g + 1

                # Heuristic function
                child.h = ((child.position[0] - self.end_node.position[0]) ** 2) + ((child.position[1] - self.end_node.position[1]) ** 2)
                child.f = child.g + child.h

                # Child is already in the open list
                if len([open_node for open_node in self.open_list if child.position == open_node.position and child.g > open_node.g]) > 0:
                    continue

                # Add the child to the open list
                heapq.heappush(self.open_list, child)


        if self.end_node.position not in self.path:
            print("No path was found")
        else:
            print("Path found")
        
        
        
    def auto_solver(self, orig, mode = "show"):
        """ Solves the maze with selected mode.
        Args:
            mode (str):  
                'print': prints the path to terminal.
                'save': saves the image with drawn path in current working directory.
                'show': shows the image with path via cv2.imshow().
        Returns:
            (void)
        """
        
        # Default mode list, mode check for valid
        mode_default = "print"
        modes_list = (mode_default, 'save', 'show')
        if not isinstance(mode, str) or mode.lower() not in modes_list:
            print(f"Mode {mode} ({type(mode)}) is not valid option {modes_list}! Assigning default value...")
            mode = mode_default
        
        mode = mode.lower()
        print(f"Mode selected: {mode}")
        
        # Pathfinding
        self.img_to_maze()
        self.solve()
        
        # Mode = print
        if mode == modes_list[0]:
            print(self.path)
            return self.path
            
        # Mode = save
        elif mode == modes_list[1]:
            self.save_path_img()
            
        # Mode = show
        elif mode == modes_list[2]:
            self.draw_inter_path(orig)
            
        
        
"""
filename = "./mazes/photo_maze.png"

img_orig = cv2.imread(filename, 0)


# HOW IT SHOULD BE WITH IMAGEPROCESSING MODULE
#
# coordinates are read from image
# start = imageprocessing.start
# end = imageprocessing.end

# start = start * scale_of_interpolation (0.07)
# end = end * scale_of_interpolation (0.07)

# For testing purposes, coords here are already scaled
start = (26, 5)
end = (4, 4)
astar = Astar(start, end, img_orig)
astar.auto_solver(mode="show")
"""

def draw_path(path, img, mask= None):
    print(img.shape,"imgshape")
    print(mask.shape, "mask")
    img = cv2.bitwise_and(img, img, mask=mask)
    for coord in path:
        img_path = cv2.circle(img, coord[::-1], 3, (0, 255, 0), -1)



    cv2.imshow("path drawn", img_path)
    cv2.waitKey(0)


def inter_path(path, scale):
    new_path = list()
    for coord in path:
        new_coord = tuple(np.divide(coord, scale).astype("int"))
        new_path.append(new_coord)

    return new_path



"""

SCALE = 0.09

#cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

img = cv2.imread("morf_th.png", 0)
width = 640
height = 480

dims = (width, height)
print(dims, "dims")
img_res = cv2.resize(img, dims, interpolation=cv2.INTER_LINEAR)

_ , img_res = cv2.threshold(img_res, 216, 255, cv2.THRESH_BINARY)
# img_res = np.array(img_res, dtype="uint8")
# cv2.imshow("sadas", img_res)
# cv2.waitKey(0)
#img_res = np.array(img_res * 255).astype("uint8")

orig = cv2.imread("frame_th.png")


start = (10, 22)
end = (30, 8)
astar = Astar(start, end, img, SCALE)

#astar.img_to_maze()
#print(astar.maze)
path = astar.auto_solver(orig, "print")


# ADJUST PATH TO CENTER OF PATH



inter_path = inter_path(path, SCALE)

#print(inter_path)
draw_path(inter_path, orig, img_res)

#draw_path(path, orig)

#astar.auto_solver(orig, "print")

#print(astar.maze)

"""
"""
SCALE = 0.09

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

img = cv2.imread("09.png", 0)
orig = cv2.imread("frame_th.png")


start = (10, 22)
end = (30, 8)
astar = Astar(start, end, img, SCALE)

astar.img_to_maze()
astar.auto_solver(orig, "show")
#astar.auto_solver(orig, "print")

#print(astar.maze)
"""











