from gopigo import *
import sys

# Default speed of gopigo2 = 200.
set_speed(100)

while True:
    a = input("Enter the Command:") # Fetch the input from the terminal
    
    if a=='w':
        fwd() # Move forward
    elif a=='a':
        left() # Turn left
    elif a=='d':
        right() # Turn Right
    elif a=='s':
        bwd() # Move back
    elif a=='t':
        increase_speed()   # Increase speed         
    elif a=='x':
        stop() # stop
    elif a=='g':
        decrease_speed()  # Decrease speed
    elif a =='n':
        left_rot()
    elif a=='m':
        right_rot()
    elif a=='l':
        turn_left(90)
    elif a=='r':
        turn_right(90)
    elif a=='z':
        stop()
        sys.exit() #quit program
    else:
        print("Wrong Command, Please Enter Again")
    time.sleep(.1)
