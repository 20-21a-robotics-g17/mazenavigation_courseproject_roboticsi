Robotics 1 Course Project : Maze Navigation
========================================
 
 **List of members**
 
 ---
 1. Shekhar Suman Patel
 2. Ilja Pavlovs
 3. Aleksandrs Rebriks
 4. Kwasi A. Boateng
 ----
 

# Overview

### Aim of the project:

* Our project aim is to drive GoPiGo based robot out of a maze using the top view camera image.

### Project steps:

* Image processing. Detection of the maze walls, robot's starting position and orientation.
* Path finding algorithm. Optimal path to the exit is being found.
* Maze solution translation. Path provided from the previous step is translated into instructions for the robot.
* Make the robot  correct its path based on the feedback from the image processing as  as well as to prevent it from driving across the black lines during the motion.

# Challenges and Solutions for robot Control

## Challenges
* The robot breakdown after a few trials of providing commands over TCP/IP protocol.
* The robot needs commands from the image processing part in order to execute movement commands
* There must be buffer inserted for the time it takes the robot to execute the command and then wait for the server to give next command

## Solutions
* The server-client code must be executed for individual trials before the robot can make it for the whole code
* The time for buffer must be selected properly
* The motion code for the robot could be enhanced/debugged

### Problems:

* Robot's dimensions should be taken into account while driving in maze.
* Perspective of the camera image should be taken into account if camera is located close to the floor.
* Accurate translation of path to the instructions for robot (units travelled by the robot).
* Select the most optimal algorithm for path finding in maze. (A*, algorithm which uses Queues).
* Control the robot remotely (wireless connection over protocol).
* *Other problems will be revealed during the working process.



# Schedule

## Maze solution algorithm (02.12.2020)

### Python 3.8 & Libraries 
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install libraries

* **NumPy** - Matrix operations
```bash
pip install numpy
```

* **opencv-python** - Image processing
```bash
pip install opencv-python
```

* **matplotlib** - Graphs and data plotting, better for image visualisation
```bash
pip install matplotlib
```
* **QueueLib** - Provide FIFO and LIFO queue implementation in python
```bash
pip install queuelib
```

### Schedule

>**04.11.2020 - 11.11.2020** (6 hours)

* Convert the 1bit image to matrix (type can be changed)
* Place starting point coordinates into the matrix
* Adjust walls' width (of matrix) to take into account robot's dimensions 
* Debug

>**11.11.2020 - 18.11.2020** (10 hours)

* Read throughout various path finding algorithms (benchmarking/testing) 
* Begin performing algorithm benchmarking and testing



>**18.11.2020 - 25.11.2020** (10 hours)

* End algorithm benchmarking 
* Select 3-5 most suitable algorithms for our project


>**25.11.2020 - 02.12.2020** (7 hours)

* Benchmark and test leftover algorithms to select the final one
* Based on selected algorithm think of the most suitable inputs/outputs types and processing



>**02.12.2020 - 09.12.2020** (13 hours)

* Start modifying the algorithm to work with our inputs and produce designated outputs (type)
* Debug and test(it won't be perfect from the beginning)



>**09.12.2020 - 16.12.2020** (15 hours)

* Continue working with algorithm to make it work
* Debugging intensifies


>**16.12.2020 - 23.12.2020** (9 hours)

* Finish the algorithm
* Helping others with debugging (also among all weeks)

* Preparing the presentation

>**23.12.2020 - 30.12.2020** (3 hours)

* Final Presentation
* (Mental breakdown)

# Image processing schedule (02.12.2020)


### Python 3.8 & Libraries 
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install libraries

* **opencv-python** - Image processing
```bash
pip install opencv-python
```
* **NumPy** - Matrix operations
```bash
pip install numpy
```
* **matplotlib** - Graphs and data plotting, better for image visualisation
```bash
pip install matplotlib
```

### Schedule

>**04.11.2020 - 11.11.2020** (7 hours)

* Take photo of the maze with starting marker (color - TBD)
* Filter the image (noise reduction)

>**11.11.2020 - 18.11.2020** (7 hours)

* Maze walls and paths detection
* Perspective transformation (based on distance to the ground)

>**18.11.2020 - 25.11.2020** (7 hours)

* Detection of robot's initial position and orientation
* Conversion to the 1bit image, coordinates of initial point are sent separately 

>**25.11.2020 - 02.12.2020** (4 hours)

* Helping others 
* Debugging based on feedback

>**02.12.2020 - 09.12.2020** (4 hours)

* Optimizing solution
* Robot's position tracking

>**09.12.2020 - 16.12.2020** (3 hours)

* Helping other with debugging
* Debugging

>**16.12.2020 - 23.12.2020** (2.5 hours)

* Preparing for the presentation


## Robot control on Command (02.12.2020)


### Schedule

>**04.11.2020 - 11.11.2020** (6 hours)

* Communication modules setup between the laptop and the robot.


>**11.11.2020 - 18.11.2020** (10 hours)

* Setting up the robot motion w.r.t. commands.


>**18.11.2020 - 25.11.2020** (10 hours)

* Feedback from the laptop if the robot has reached the required location.

>**25.11.2020 - 02.12.2020** (7 hours)

* Feedback from the robot using odometers if the sync between the given position and the camera position from image processing is the  same.

>**02.12.2020 - 09.12.2020** (13 hours)

* Making robot not to cross the path using the camera on the robot.


>**09.12.2020 - 16.12.2020** (15 hours)

* Hardware and software combining and debugging.


>**16.12.2020 - 23.12.2020** (9 hours)

* Making robot to get out of the maze successfully.


>**23.12.2020 - 30.12.2020** (3 hours)

* Final Presentation

**Component list**

Item| Link to the item | We will provide | Need from instructors | Total |
--- | --- | --- | --- |--- 
Camera| https://www.logitech.com/en-us/product/brio?crid=34| 0 | 1| 1
Camera mount | https://www.amazon.com/Mount-Overhead-Camera-Mounting-Capacity/dp/B07QDKJ2VH | 0 | 1 |1 
GoPiGo robot | https://www.dexterindustries.com/gopigo3/ | 0 | 1 |1 
SD card | https://www.euronics.ee/t-en/92907/photo-and-video/microsdxc-memory-card-sandisk-extreme-adapter-rescue-pro-deluxe-(64-gb)/sdsqxa2-064g-gn6ma | 0 | 1 |1
Laptop | https://www.euronics.ee/t-en/102055/it/notebook-hp-pavilion-15-cw1005no/13j55ea-uuw | 0 | 1 |1 
USB cable for the camera| https://www.amazon.co.uk/StarTech-com-6in-Micro-USB-Cable-Black/dp/B003YKX6WM | 0 | 1 |1 
Black tape| https://www.amazon.in/ETI-Colour-Tape-Inch-BLACK/dp/B01D2GWZCY | 0 | 1 |1 
White sheets of paper or canvas (~8m*8m)| https://www.amazon.in/ETI-Colour-Tape-Inch-BLACK/dp/B01D2GWZCY | 0 | 1 |1 

 ----
 
 **Challenges and Solutions**
 
Problems in image processing step:

- Develop automatic maze exit detection method instead of manual. It could be based on the analysis of external contour.
- Develop an image processing pipeline which is more robust to the variations of lighting conditions.
- Exclude robot image from starting position so that it doesn't conflict with the maze wall detection.
- Finish maze corner estimation method
- Develop robot marker detection

 Maze solutions developmental problems
During the development, two main variants of A* python implementations were used, 
consisting of simple and more advanced code logic. The speed between simple and advanced variants is crucial. 
Advanced algorithm was able to solve (with drawing path on original maze image) from 20x20px to 2000x2000px 
mazes incredibly fast (not enough RAM for larger mazes), when the simple algorithm was much slower, in fact for 
20x20px maze image it was calculating for 2 hours and still wasn’t ready.

- Bad inputs. The advanced A* implementation was designed to operate on open mazes, with entrances placed on the top row of the bitmap image [0..1] and exit on the bottom row of the image. However, when specific start and end points are selected it crashes. Due to complexity, the debugging is not quite easy and is very time consuming.
- Enhance the efficiency of simple implementation. Solution to first problem could be optimizing the calculations in simple A* implementation.

