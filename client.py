import socket
from gopigo import *
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '192.168.43.191'  # ip address of the server, Must change
port = 12345
s.connect((host, port))

while True:
    command = input('Enter your command:')
    #     command1 = bytes(command, 'utf-8')
    s.send(command.encode())
    reply = s.recv(1024)
    a = reply.decode('utf-8')
    if a == 'Terminate':
        break
    print(reply.decode('utf-8'))

    if a == 'w':
        fwd()  # Move forward
    elif a == 'a':
        left()  # Turn left
    elif a == 'd':
        right()  # Turn Right
    elif a == 's':
        bwd()  # Move back
    elif a == 't':
        increase_speed()  # Increase speed
    elif a == 'x':
        stop()  # stop
    elif a == 'g':
        decrease_speed()  # Decrease speed
    elif a == 'n':
        left_rot()
    elif a == 'm':
        right_rot()
    elif a == 'z':
        stop()
        sys.exit()  # quit program
    else:
        print("Wrong Command, Please Enter Again")
    time.sleep(.1)

s.close()
