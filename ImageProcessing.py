import cv2
import numpy as np


class ImageProcessing:
    """A class for applying image processing and extracting maze, robot position.

    Attributes:
        inter_sc (float): scaling factor for interpolation
        mark (numpy.ndarray): grayscale image of robot's marker used as the reference
        img (numpy.ndarray): captured frame

    Methods:
        get_pos():
            returns position (x,y) of the robot
        get_rot(method):
            returns rotation of the robot in degrees estimated by selected method (shape, area)
        get_maze():
            returns thresholded binary downscaled maze image
        get_exit():
            returns exit coordinate
        display():
            visualizes maze image processing step solution
    """
    def __init__(self, inter_sc, img=-1, mark=-1):
        self.img = img
        self.mark = mark
        self.robot_found = False
        self.inter_sc = inter_sc

    def get_pos(self):
        def detect_robot(img):

            # thresholding

            hls = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
            low = np.array([54, 59, 45])
            high = np.array([84, 200, 255])
            thresh = cv2.inRange(hls, low, high)

            # contour filtering
            cnts = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]

            r_cnt = []
            for i, cnt in enumerate(cnts):
                if cnt.size > 100 and cnt.size < 10000:
                    r_cnt = cnts[i]
                    r_cnti = i

            if len(r_cnt)==0:
                #robot not found
                return -1,-1

            filled_c = np.zeros_like(thresh)
            cv2.drawContours(filled_c, cnts, r_cnti, (255, 255, 255), thickness=cv2.FILLED)

            return r_cnt, filled_c

        # get robot contour and filled contour

        self.robot_cnt, self.filled_c = detect_robot(self.img)

        # robot detected / not detected

        if np.amax(self.filled_c) == -1:
            self.robot_found = False
            return
        else:
            self.robot_found = True

        # get robot coordinates

        (self.xr, self.yr), self.rad = cv2.minEnclosingCircle(self.robot_cnt)
        self.xr, self.yr, self.rad = int(round(self.xr)), int(round(self.yr)), int(round(self.rad))
        # self.yr, self.xr = np.mean(np.nonzero(self.filled_c),axis=1)
        return (self.xr, self.yr)

    def get_rot(self, method="shape", coords=[0,0]):
        def rob_rotation(mark, cnt, thresh, alg, coords):

            mark = 255 - mark  # invert marker image

            if alg == "area":

                # cut ROI

                rect_x, rect_y, rect_w, rect_h = cv2.boundingRect(cnt)
                roi = thresh[rect_y:rect_y + rect_h, rect_x:rect_x + rect_w]

                # rec2square

                roi = rec2square(roi)

                # scaling

                ref_marker_size = np.count_nonzero(mark)  # reference marker size
                rec_marker_size = np.count_nonzero(roi)  # recognized marker size

                sc_f = (np.sqrt(ref_marker_size / rec_marker_size))
                dim = (int(round(roi.shape[0] * sc_f)), int(round(roi.shape[1] * sc_f)))  # resized dimensions
                roi = cv2.resize(roi, dim)

                # centroid alignment

                rcontours = cv2.findContours(roi, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
                r_cnt = rcontours[0]
                M = cv2.moments(r_cnt)
                rcentroid = np.array([int(M['m01'] / M['m00']), int(M['m10'] / M['m00'])])

                mcontours = cv2.findContours(mark, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
                m_cnt = mcontours[0]
                M = cv2.moments(m_cnt)
                mcentroid = np.array([int(M['m01'] / M['m00']), int(M['m10'] / M['m00'])])

                mark_h, mark_w = mark.shape
                roi_h, roi_w = roi.shape

                if mark_h > roi_h:
                    roi = cv2.copyMakeBorder(roi, 0, mark_h - roi_h, 0, mark_h - roi_h, cv2.BORDER_CONSTANT,
                                             value=(0, 0, 0))
                    roi = translate(roi, (mcentroid - rcentroid))
                else:
                    roi = translate(roi, (mcentroid - rcentroid))
                    roi = roi[:roi_h - mark_h, :roi_h - mark_h]

                # rotation estimation

                max_area = -np.inf
                theta = 0

                for a in range(360):
                    rotated = rotate(roi, a, scale=1.3)  # mask
                    non_zero = np.count_nonzero(cv2.bitwise_and(mark, mark, mask=rotated))
                    if non_zero > max_area:
                        max_area = non_zero
                        theta = a

                return theta

            elif alg == "shape":

                # marker contours

                mcontours = cv2.findContours(mark, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
                m_cnt = mcontours[0]

                # rotation estimation
                closest_match = np.inf

                for a in range(360):
                    rotated = rotate_contour(cnt, a)
                    match = cv2.matchShapes(rotated, m_cnt, cv2.CONTOURS_MATCH_I2, 0.0)
                    if match < closest_match:
                        closest_match = match
                        theta = a

                return theta

            elif alg == "coord":
                # coords -> [x,y]
                return np.atan(self.yr-coords[1])/(self.xr - coords[0])




        def rec2square(img):  # add padding of 0's keeping original img in the center or almost in the center
            y, x = img.shape
            padd = np.abs(y - x) / 2
            if (x + y) % 2 == 0:
                even = True
            else:
                even = False
            if y > x:
                if even:
                    padd = int(padd)
                    img = cv2.copyMakeBorder(img, 0, 0, padd, padd, cv2.BORDER_CONSTANT, value=(0, 0, 0))
                else:
                    padd = int(np.ceil(padd))
                    padd2 = (x + 2 * padd - y)
                    padd21 = int(np.floor(padd2 / 2))
                    padd22 = padd2 - padd21
                    img = cv2.copyMakeBorder(img, padd21, padd22, padd, padd, cv2.BORDER_CONSTANT, value=(0, 0, 0))
            else:
                if even:
                    padd = int(padd)
                    img = cv2.copyMakeBorder(img, padd, padd, 0, 0, cv2.BORDER_CONSTANT, value=(0, 0, 0))
                else:
                    padd = int(np.ceil(padd))
                    padd2 = (y + 2 * padd - x)
                    padd21 = int(np.floor(padd2 / 2))
                    padd22 = padd2 - padd21
                    img = cv2.copyMakeBorder(img, padd, padd, padd21, padd22, cv2.BORDER_CONSTANT, value=(0, 0, 0))
            return img

        def translate(img, vect):
            h, w = img.shape
            t_x, t_y = vect
            M = np.float32([[1, 0, t_x], [0, 1, t_y]])
            dst = cv2.warpAffine(img, M, (w, h))
            return dst

        def rotate(image, angle, center=None, scale=1.0):
            (h, w) = image.shape[:2]

            if center is None:
                center = (w / 2, h / 2)

            # Perform the rotation
            M = cv2.getRotationMatrix2D(center, angle, scale)
            rotated = cv2.warpAffine(image, M, (w, h))

            return rotated

        def cart2pol(x, y):
            theta = np.arctan2(y, x)
            rho = np.hypot(x, y)
            return theta, rho

        def pol2cart(theta, rho):
            x = rho * np.cos(theta)
            y = rho * np.sin(theta)
            return x, y

        def rotate_contour(cnt, angle):
            M = cv2.moments(cnt)
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])

            cnt_norm = cnt - [cx, cy]

            coordinates = cnt_norm[:, 0, :]
            xs, ys = coordinates[:, 0], coordinates[:, 1]
            thetas, rhos = cart2pol(xs, ys)

            thetas = np.rad2deg(thetas)
            thetas = (thetas + angle) % 360
            thetas = np.deg2rad(thetas)

            xs, ys = pol2cart(thetas, rhos)

            cnt_norm[:, 0, 0] = xs
            cnt_norm[:, 0, 1] = ys

            cnt_rotated = cnt_norm + [cx, cy]
            cnt_rotated = cnt_rotated.astype(np.int32)

            return cnt_rotated

        if self.robot_found:
            self.angle = rob_rotation(self.mark, self.robot_cnt, self.filled_c, method, coords)
            return self.angle
        else:
            return

    def get_maze(self):

        def epf(img):
            # Edge preserving filter
            dst = cv2.bilateralFilter(src=img, d=0, sigmaColor=100, sigmaSpace=15)
            kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]], np.float32)  # sharpen kernel
            blur = cv2.filter2D(dst, -1, kernel)
            return blur

        def thr(img):
            # THRESHOLDING
            gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
            thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]  # adaptive OTSU thresholding
            return thresh

        def morph(img):
            # MORPHOLOGICAL OPERATIONS
            k4 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 5))
            k5 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 9))
            opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, k4, iterations=2)
            opening = cv2.morphologyEx(opening, cv2.MORPH_OPEN, k5, iterations=2)
            return opening

        def find_maze(morphed):
            # Edge detection
            edges = cv2.GaussianBlur(morphed, (3, 3), 1)
            edges = cv2.Canny(edges, 255, 255)

            # CONTOUR FILTERING
            im_y = morphed.shape[0]  # height
            im_x = morphed.shape[1]  # width
            sz = im_y * im_x  # area
            pad = 20  # padding
            contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            cacc = 0.5  # center position accuracy

            for cnt in contours:

                epsilon = 0.08 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                x, y, w, h = cv2.boundingRect(approx)

                if w * h > sz * 0.3 and w / h > 0.5 and w / h < 1.5 \
                        and im_y / 2 - cacc * im_y <= y <= im_y / 2 + cacc * im_y \
                        and im_x / 2 - cacc * im_x <= x <= im_x / 2 + cacc * im_x:
                    maze = np.zeros_like(morphed)
                    maze[y:y + h, x:x + w] = morphed[y:y + h, x:x + w]
                    maze_contour = cnt
                    bounding = [[y - pad, y + h + 1 * pad], [x - pad, x + w + 1 * pad]]  # bounding coordinates

                    #approximated
                    maze_approx = approx

            return maze, maze_contour, bounding, maze_approx

        """
        def find_exit(maze, cnt, bound):
            x, y, w, h = cv2.boundingRect(cnt)
            po = 5
            pi = 0

            extended = maze[y:y+h, x:x+w]
            extended = cv2.copyMakeBorder(extended, po,po,po,po,cv2.BORDER_REPLICATE)
 
            maze_like = np.zeros_like(maze)
            maze[y-po:y+h+po, x-po:x+w+po] = extended

            mask = np.zeros_like(maze)
            cv2.rectangle(mask, (x, y), (x + w+2*po, y + h+2*po), (255, 255, 255), thickness=-1)
            cv2.rectangle(mask, (x+po+pi, y+po+pi), (x+w+po-pi, y+h+po-pi), (0,0,0), thickness=-1)
            
            mask = cv2.bitwise_and(maze, maze, mask = mask)
            
            yc,xc = np.mean(np.nonzero(mask), axis=1)
            yc = int(round(yc))
            xc = int(round(xc))

            exit_x = int((abs(xc-po) + xc-po) / 2)
            exit_y = int((abs(yc-po) + yc-po) / 2) #ReLU

            return (exit_x, exit_y)
        """

        def find_exit(maze, img):
            #maze = cv2.bitwise_and(img, img, mask = maze)

            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            low = np.array([23, 175, 0])
            high = np.array([30, 255, 255])
            thresh = cv2.inRange(hsv, low, high)

            exit_y, exit_x = np.mean(np.nonzero(thresh), axis=1)

            return (int(exit_x), int(exit_y))

        def interpolate(maze, sc):
            # interpolation
            width = int(maze.shape[1] * sc)
            height = int(maze.shape[0] * sc)
            dim = (width, height)
            img_resized = cv2.resize(maze, dim, interpolation=cv2.INTER_AREA)
            # img_gray = cv2.cvtColor(img_resized, cv2.COLOR_BGR2GRAY)

            ret, thresholded = cv2.threshold(img_resized, 216, 255, cv2.THRESH_BINARY_INV)

            # Morphology for adjusting walls and path sizes, removing artifacts
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            thresholded = cv2.dilate(thresholded, kernel, iterations=1)
            thresholded = cv2.morphologyEx(thresholded, cv2.MORPH_CLOSE, kernel)

            maze = thresholded / 255.0
            return maze


        # filter

        blur = epf(self.img)

        # threshold

        thresh = thr(blur)

        if self.robot_found:
            # remove robot

            rad_sc = int(round(self.rad * .5))
            k1 = cv2.getStructuringElement(cv2.MORPH_RECT, (rad_sc, rad_sc)) #kernels
            k3 = cv2.getStructuringElement(cv2.MORPH_CROSS, (7, 7))
            k4 = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 5))
            k5 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 2))

            mask_r = cv2.dilate(self.filled_c, k1, iterations=2)
            cv2.rectangle(mask_r, (self.xr - self.rad - 7, self.yr - self.rad - 7),
                          (self.xr + self.rad + 7, self.yr + self.rad + 7), (255, 255, 255), thickness=-1)
            thresh = thresh + mask_r

            # remove robot's shadow and left artifacts

            thresh = cv2.dilate(thresh, k3)
            thresh = cv2.medianBlur(thresh, 5)
            thresh = cv2.erode(thresh, k4)
            thresh = cv2.erode(thresh, k5)

        # morphology

        morphed = morph(thresh)

        # maze detection

        self.maze, self.maze_cnt, self.maze_bound, self.maze_approx = find_maze(morphed)

        # maze interpolation

        self.int_maze = interpolate(self.maze, self.inter_sc)

        #self.exit_c = find_exit(self.maze, self.maze_approx, self.maze_bound)
        self.exit_c = find_exit(self.maze, self.img)

        return self.int_maze

    def get_exit(self):
        try:
            return self.exit_c
        except:
            print("Exit coordinates are not defined yet")
            return -1

    def display(self, mode="pos"):
        if mode == "pos":
            image = self.img.copy()
            image = cv2.bitwise_and(image, image, mask = self.maze)
            if self.robot_found:
                cv2.putText(image,
                            str(self.angle) + " deg " + str(self.xr) + " X " + str(self.yr) + " Y",
                            (self.xr - self.rad, self.yr + self.rad + 20),
                            cv2.FONT_HERSHEY_SIMPLEX, .5, (100, 0, 255), 1, cv2.LINE_AA)
                cv2.circle(image, (self.xr, self.yr), self.rad, (0, 0, 255), 1)

                #arrow
                px = int(round(self.xr+2*self.rad*np.sin(np.radians(self.angle))))
                py = int(round(self.yr-2*self.rad*np.cos(np.radians(self.angle))))
                cv2.arrowedLine(image, (self.xr, self.yr), (px, py), (0, 230, 230), 2, tipLength=.2)

            #exit
            cv2.circle(image, (self.exit_c[0], self.exit_c[1]), 20, (0,230,230), -1)
            cv2.putText(image,
                        str(self.exit_c) + " EXIT ",
                        (self.exit_c[0] - 30, self.exit_c[1] + 30),
                        cv2.FONT_ITALIC, .5, (100, 0, 255), 1, cv2.LINE_AA)

            cv2.imshow("Position estimation", image)
            cv2.waitKey(0)

        if mode == "maze":
            cv2.imshow("Thresholded maze", self.maze)
            cv2.waitKey(0)



if __name__ == '__main__':

    # outside the loop
    ip = ImageProcessing(inter_sc=.07)  # image processing module
    ip.mark = cv2.imread("mark.png", 0) # marker image reading

    # inside the loop
    ip.img = cv2.imread("test_image2tape.png")

    # continuous
    robot_pos = ip.get_pos()
    print(int(robot_pos[0]*0.07), int(robot_pos[1]*0.07))
    robot_rot = ip.get_rot("area")
    print(robot_rot)

    # one time run
    maze = ip.get_maze()
    print(int(ip.exit_c[0] * 0.07), int(ip.exit_c[1] * 0.07))

    #visualisation
    ip.display("pos")

    # #rot func
    # rot = [] #outside the loop
    #
    # #loop
    # rot.append([self.xr, self.yr])
    #
    # if len(rot)<10:
    #     robot_rot = ip.get_rot(method="coords", rot[-1])
    # else:
    #     rot.pop(0)
    #     robot_rot = ip.get_rot(method="coords", rot[-1])