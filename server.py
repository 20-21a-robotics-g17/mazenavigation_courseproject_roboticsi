import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '192.168.43.191'  # ip address of server, Must change
port = 12345

try:
    s.bind((host, port))
except socket.error:
    print('Bind failed')
else:
    s.listen(5)
    print(' Socket awaiting messages')
    (conn, addr) = s.accept()
    print(' Connected')
    while True:
        data = conn.recv(1024)
        data = data.decode('utf-8')
        print('I sent a message back in response to:', data)

        # Process solution from maze algorithm
        # Sample message below. Script will be edited after getting hands on maze algorithm output.
        algo_output = ''  # Variable for algorithm output.
        if algo_output == 'forward':
            reply = 'w'
        elif algo_output == 'backward':
            reply = 's'
        elif algo_output == 'Terminate':
            reply = 'Terminate'
        else:
            reply = 'unknown command'

        conn.send(reply.encode())
